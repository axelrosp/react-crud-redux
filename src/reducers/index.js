import { combineReducers } from "redux";
import productosReducer from "./productosReducer";
import alertaReducer from "./alertReducer";

export default combineReducers({
	productos: productosReducer,
	alerta: alertaReducer,
});
