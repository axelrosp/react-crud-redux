import {
	AGREGAR_PRODUCTO,
	AGREGAR_PRODUCTO_ERROR,
	AGREGAR_PRODUCTO_EXITO,
	IS_FETCHING_PRODUCTS,
	FETCHING_PRODUCTS_EXITO,
	FETCHING_PRODUCTS_ERROR,
	GET_REMOVE_PRODUCT,
	REMOVED_PRODUCT_ERROR,
	REMOVED_PRODUCT_EXITO,
	EDITED_PRODUCT_ERROR,
	EDITED_PRODUCT_EXITO,
	GET_EDIT_PRODUCT,
} from "../types";

const initialState = {
	productos: [],
	error: null,
	loading: false,
	removeProduct: null,
	editProduct: null,
};

export default function (state = initialState, action) {
	switch (action.type) {
		case AGREGAR_PRODUCTO:
		case IS_FETCHING_PRODUCTS:
			return {
				...state,
				loading: action.payload,
			};
		case AGREGAR_PRODUCTO_EXITO:
			return {
				...state,
				loading: false,
				productos: [...state.productos, action.payload],
			};
		case EDITED_PRODUCT_ERROR:
		case FETCHING_PRODUCTS_ERROR:
		case AGREGAR_PRODUCTO_ERROR:
			return {
				...state,
				loading: false,
				error: action.payload,
			};
		case FETCHING_PRODUCTS_EXITO:
			return {
				...state,
				loading: false,
				error: null,
				productos: action.payload,
			};
		case GET_REMOVE_PRODUCT:
			return {
				...state,
				removeProduct: action.payload,
			};
		case REMOVED_PRODUCT_EXITO:
			return {
				...state,
				productos: state.productos.filter((p) => p.id !== state.removeProduct),
				removeProduct: null,
			};
		case REMOVED_PRODUCT_ERROR:
			return {
				...state,
				removeProduct: null,
				error: true,
			};
		case GET_EDIT_PRODUCT:
			return {
				...state,
				editProduct: action.payload,
			};
		case EDITED_PRODUCT_EXITO:
			return {
				...state,
				editProduct: null,
				productos: state.productos.map((product) => {
					return product.id === action.payload.id ? (product = action.payload) : product;
				}),
			};
		default:
			return state;
	}
}
