import React, { useState } from "react";
import { createProductAction } from "../actions/productoActions";
import { useDispatch, useSelector } from "react-redux";
import { showAlertAction, hideAlertAction } from "../actions/alertaActions";

//History se pasa automaticamente al estar dentro de react-router-dom y sirve para redireccionar
const NuevoProducto = ({ history }) => {
	const [product, setProduct] = useState({ nombre: "", precio: "0" });
	const { nombre, precio } = product;

	//Acceder al redux del store
	const loading = useSelector((state) => state.productos.loading);
	const error = useSelector((state) => state.productos.error);
	const alerta = useSelector((state) => state.alerta.alerta);
	console.log(alerta);

	//Redux
	const dispatch = useDispatch();
	const addProduct = (product) => dispatch(createProductAction(product));

	const handleSubmit = (e) => {
		e.preventDefault();

		if (nombre.trim() === "" || precio.trim() === "") {
			const alert = {
				msg: "Ambos campos son obligatorios",
				class: "alert alert-danger text-center text-uppercase p3",
			};
			dispatch(showAlertAction(alert));
			return;
		}
		dispatch(hideAlertAction());

		addProduct(product);
		setProduct({
			nombre: "",
			precio: 0,
		});

		history.push("/");
	};

	const handleChange = (e) => {
		setProduct({ ...product, [e.target.name]: e.target.value });
	};

	return (
		<div className="row justify-content-center">
			<div className="col-md-8">
				<div className="card">
					<div className="card-body">
						<h2 className="text-center mb-4 font-weight-bold">Agregar nuevo producto</h2>
						{alerta ? <p className={alerta.class}>{alerta.msg}</p> : null}
						<form onSubmit={handleSubmit}>
							<div className="form-group">
								<lable>Nombre producto</lable>
								<input
									type="text"
									className="form-control"
									placeholder="Nombre producto"
									name="nombre"
									value={nombre}
									onChange={handleChange}
								/>
							</div>
							<div className="form-group">
								<lable>Precio</lable>
								<input type="number" className="form-control" placeholder="Precio" name="precio" value={precio} onChange={handleChange} />
							</div>
							<button type="submit" className="btn btn-primary font-weifht-bold text-uppercase d-block w-100">
								Agregar
							</button>
						</form>
						{loading ? <p>Cargando... </p> : null}
						{error ? <p className="alert alert-danger p2 mt-4">Hubo un error</p> : null}
					</div>
				</div>
			</div>
		</div>
	);
};

export default NuevoProducto;
