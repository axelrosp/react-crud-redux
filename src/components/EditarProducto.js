import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { editProductAction } from "../actions/productoActions";
import { useHistory } from "react-router-dom";
const EditarProducto = () => {
	const [product, setProduct] = useState({
		id: "",
		nombre: "",
		precio: "",
	});
	const { nombre, precio } = product;
	const dispatch = useDispatch();
	const history = useHistory();
	//producto a editar
	const productEditar = useSelector((state) => state.productos.editProduct);

	useEffect(() => {
		setProduct(productEditar);
	}, [productEditar]);

	const handleSubmit = (e) => {
		console.log("hola");
		e.preventDefault();
		dispatch(editProductAction(product));
		history.push("/");
	};

	const handleChange = (e) => {
		setProduct({ ...product, [e.target.name]: e.target.value });
	};

	return (
		<div className="row justify-content-center">
			<div className="col-md-8">
				<div className="card">
					<div className="card-body">
						<h2 className="text-center mb-4 font-weight-bold">Editar producto</h2>
						<form onSubmit={handleSubmit}>
							<div className="form-group">
								<lable>Nombre producto</lable>
								<input
									type="text"
									className="form-control"
									placeholder="Nombre producto"
									value={nombre}
									name="nombre"
									onChange={handleChange}
								/>
							</div>
							<div className="form-group">
								<lable>Precio producto</lable>
								<input
									type="number"
									className="form-control"
									placeholder="Precio producto"
									value={precio}
									name="precio"
									onChange={handleChange}
								/>
							</div>
							<button type="submit" className="btn btn-primary font-weifht-bold text-uppercase d-block w-100">
								Guardar cambios
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	);
};

export default EditarProducto;
