import React from "react";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { removeProductAction, getEditProductAction } from "../actions/productoActions";
import Swal from "sweetalert2";

const Product = ({ product }) => {
	const { id, nombre, precio } = product;

	const dispatch = useDispatch();
	//habilitar history para redireccion
	const history = useHistory();

	//Confirmar si desea eliminar
	const confirmRemoveProduct = () => {
		//Preguntar al user
		Swal.fire({
			title: "¿Estas seguro?",
			text: "Una vez eliminado no se puede recuperar",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Si, eliminar!",
			cancelButtonText: "Cancelar",
		}).then((result) => {
			if (result.value) {
				//Pasar al action
				dispatch(removeProductAction(id));
			}
		});
	};

	//Redirigir de forma programada
	const redirectEdit = () => {
		dispatch(getEditProductAction(product));
		history.push(`/productos/editar/${id}`);
	};

	return (
		<tr>
			<td>{nombre}</td>
			<td>
				<span className="font-weight-bold">{precio}€</span>
			</td>
			<td className="acciones">
				<button type="button" className="btn btn-primary mr-2" onClick={redirectEdit}>
					Editar
				</button>

				<button type="button" className="btn btn-danger" onClick={confirmRemoveProduct}>
					Eliminar
				</button>
			</td>
		</tr>
	);
};

export default Product;
