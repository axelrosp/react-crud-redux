import {
	AGREGAR_PRODUCTO,
	AGREGAR_PRODUCTO_ERROR,
	AGREGAR_PRODUCTO_EXITO,
	IS_FETCHING_PRODUCTS,
	FETCHING_PRODUCTS_EXITO,
	FETCHING_PRODUCTS_ERROR,
	GET_REMOVE_PRODUCT,
	REMOVED_PRODUCT_EXITO,
	REMOVED_PRODUCT_ERROR,
	EDITED_PRODUCT_ERROR,
	EDITED_PRODUCT_EXITO,
	GET_EDIT_PRODUCT,
} from "../types";
import axiosClient from "../config/axios";
import Swal from "sweetalert2";

//Crear nuevos productos
export function createProductAction(product) {
	return async (dispatch) => {
		dispatch(addProduct());

		try {
			//insertar en la api
			const result = await axiosClient.post("/productos", product);
			dispatch(addProductSuccess(result));
			//Alerta success
			Swal.fire("Correcto", "El producto se agrego correctamente", "success");
		} catch (error) {
			console.log(error);
			dispatch(addProductError(error));
			//Alerta error
			Swal.fire({
				icon: "error",
				title: "Hubo un error",
				text: "Hubo un error, intenta de nuevo",
			});
		}
	};
}

const addProduct = () => ({
	type: AGREGAR_PRODUCTO,
	payload: true,
});

//If product added to database its added in redux
const addProductSuccess = (product) => ({
	type: AGREGAR_PRODUCTO_EXITO,
	payload: product,
});

const addProductError = (error) => ({
	type: AGREGAR_PRODUCTO_ERROR,
	payload: error,
});

//Fetch products
export function fetchProductsAction() {
	return async (dispatch) => {
		dispatch(isFetchingProducts());
		try {
			const response = await axiosClient.get("/productos/");
			dispatch(fetchingProductsSuccess(response.data));
		} catch (error) {
			dispatch(fetchingProductsError(error));
		}
	};
}

const isFetchingProducts = () => ({
	type: IS_FETCHING_PRODUCTS,
	payload: true,
});

const fetchingProductsSuccess = (products) => ({
	type: FETCHING_PRODUCTS_EXITO,
	payload: products,
});

const fetchingProductsError = () => ({
	type: FETCHING_PRODUCTS_ERROR,
	payload: true,
});

//Selecciona y elimina un producto
export function removeProductAction(id) {
	return async (dispatch) => {
		dispatch(getRemoveProduct(id));

		try {
			await axiosClient.delete(`/productos/${id}`);
			dispatch(removeProductSuccess());
			//Si se elimina mostrar alerta
			Swal.fire("Eliminado!", "El producto ha sido eliminado correctamente.", "success");
		} catch (error) {
			dispatch(removeProductError());
		}
		console.log(id);
	};
}

const getRemoveProduct = (id) => ({
	type: GET_REMOVE_PRODUCT,
	payload: id,
});

const removeProductSuccess = () => ({
	type: REMOVED_PRODUCT_EXITO,
});

const removeProductError = () => ({
	type: REMOVED_PRODUCT_ERROR,
});

//Editar producto
export function getEditProductAction(product) {
	return (dispatch) => {
		dispatch(getEditProduct(product));
	};
}

const getEditProduct = (product) => ({
	type: GET_EDIT_PRODUCT,
	payload: product,
});
//Edita un registro en la api y en el state
export function editProductAction(product) {
	return async (dispatch) => {
		try {
			const result = await axiosClient.put(`/productos/${product.id}`, product);
			dispatch(editProductSuccess(result));
		} catch (error) {
			dispatch(editProductError());
		}
	};
}

const editProductSuccess = (product) => ({
	type: EDITED_PRODUCT_EXITO,
	payload: product,
});

const editProductError = (product) => ({
	type: EDITED_PRODUCT_ERROR,
	payload: true,
});
